#!/usr/bin/env python2
from aoc import *

if test_mode:
    data = """R8,U5,L5,D3
U7,R6,D4,L4
""".strip()

lines = list(each_line(data))

ADDENDS = {
    'L': (-1, 0),
    'R': (1, 0),
    'U': (0, 1),
    'D': (0, -1)
}

paths = []
counts = []
for l in lines:
    cur = (0,0)
    path = [cur]
    steps = {}
    step = 0
    steps[cur] = step
    for move in l.split(','):
        direction = move[0]
        length = int(move[1:])
        a = ADDENDS[direction]

        for i in range(length):
            cur = cur[0]+a[0], cur[1] + a[1]
            path.append(cur)
            step += 1
            if cur not in steps:
                steps[cur] = step
    paths.append(path)
    counts.append(steps)

short = 0xfffffffffffffffff
steps = 0xfffffffffffffffff
for i in set(paths[0]).intersection(set(paths[1])):
    if i == (0,0):
        continue
    short = min(abs(i[0])+abs(i[1]), short) 
    steps = min(counts[0][i]+counts[1][i], steps)
print short
print steps
