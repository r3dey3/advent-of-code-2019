#!/usr/bin/env python2
from aoc import *

if test_mode:
    data = """
3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99
""".strip()

PROG = [ int(i) for i in list(each_line(data))[0].split(',') ]

INPUT_COUNT = {
    1:2,
    2:2,
    3:0,
    4:1,
    5:2,
    6:2,
    7:2,
    8:2,
    99: 0
}
OUTPUT_COUNT = {
    1:1,
    2:1,
    3:1,
    4:0,
    5:0,
    6:0,
    7:1,
    8:1,
    99: 0
}


def decode(prog, cur):
    all_f = prog[cur] / 100
    instr = prog[cur] % 100
    flags = []
    for i in range(3):
        flags.append(all_f % 10)
        all_f = all_f / 10
    p = []
    for i in range(INPUT_COUNT[instr]):
        if flags[i]:
            p.append(prog[cur+i+1])
        else:
            p.append(prog[prog[cur+i+1]])
    for i in range(OUTPUT_COUNT[instr]):
        p.append(prog[cur+i+1+INPUT_COUNT[instr]])

    #print instr, p, cur + 1 + INPUT_COUNT[instr] + OUTPUT_COUNT[instr]

    return instr, p, cur + 1 + INPUT_COUNT[instr] + OUTPUT_COUNT[instr]

def run_prog(input):
    prog = list(PROG)
    run = True
    cur = 0
    cur_in = 0
    while run:
        inst, params, cur = decode(prog, cur)
        if inst == 1:
            #add
            prog[params[2]] = params[0] + params[1]
        elif inst == 2:
            #mul
            prog[params[2]] = params[0] * params[1]
        elif inst == 3:
            prog[params[0]] = input[cur_in]
            cur_in += 1
        elif inst == 4:
            print params[0]
        elif inst == 5:
            if params[0]:
                cur = params[1]
        elif inst == 6:
            if not params[0]:
                cur = params[1]
        elif inst == 7:
            if params[0] < params[1]:
                prog[params[2]] = 1
            else:
                prog[params[2]] = 0
        elif inst == 8:
            if params[0] == params[1]:
                prog[params[2]] = 1
            else:
                prog[params[2]] = 0

        elif inst == 99:
            run = False
        else:
            raise Exception('bad')
    
    return prog[0]

#print decode(1002)
run_prog([1])

run_prog([5])


