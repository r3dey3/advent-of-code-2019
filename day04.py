#!/usr/bin/env python2
from aoc import *

count = 0
for i in range(156218,652527+1):
    digits = [int(j) for j in str(i)]
    found_dup = False
    bad = False
    prev = digits[0]
    for j in digits[1:]:
        if prev > j:
            bad = True
            break
        if prev == j:
            found_dup = True
        prev = j

    if found_dup and not bad:
        count += 1

print count
            

count = 0
for i in range(156218,652527+1):
    digits = [int(j) for j in str(i)]
    found_dup = False
    bad = False
    prev = digits[0]

    for j in digits[1:]:
        if prev > j:
            bad = True
            break
        prev = j

    if bad:
        continue

    found = any(digits.count(i) == 2 for i in set(digits))

    if found:
        count += 1

print count
            



