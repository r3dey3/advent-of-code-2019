#!/usr/bin/env python2
from aoc import *

if test_mode:
    data = """
    2,4,4,5,99,0
""".strip()

PROG = [ int(i) for i in list(each_line(data))[0].split(',') ]

def run_prog(noun, verb):
    d = list(PROG)
    d[1] = noun
    d[2] = verb
    run = True
    cur = 0
    while run:
        if d[cur] == 1:
            #add
            d[d[cur+3]] = d[d[cur+1]] + d[d[cur+2]]
            cur += 4
        elif d[cur] == 2:
            #mul
            d[d[cur+3]] = d[d[cur+1]] * d[d[cur+2]]
            cur += 4
        elif d[cur] == 99:
            run = False
        else:
            raise Exception('bad')

    return d[0]
print run_prog(12,2)


for i in range(100):
    for j in range(100):
        if run_prog(i,j) == 19690720:
            print 100*i + j

