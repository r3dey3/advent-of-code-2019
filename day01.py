#!/usr/bin/env python2
from aoc import *

if test_mode:
    data = """100756
""".strip()

lines = list(int(i) for i in each_line(data))

tot_0 = 0
tot = 0
for l in lines:
    fuel = (l//3)-2
    tot_0 += fuel
    while fuel > 0:
        tot += fuel
        fuel = (fuel//3)-2

print tot_0
print tot
    #idx, groups = match(l, "re1", "re2", '(.*)')
